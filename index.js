//console.log("hello world");

	
	/*1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function
		*/

		function act1(num1,num2){
			let sum=  num1+num2
			console.log("display sum of " + num1 + " and " + num2);
			console.log(sum);
		};
		act1(5,15);

		function act2(num3,num4){
			let sum1=  num3-num4
			console.log("display difference of " + num3 + " and " + num4);
			console.log(sum1);
		};
		act2(20,5);

		function act3(num5,num6){
		
			console.log("display product of " + num5 + " and " + num6)
		return num5*num6
			//let sum2 = num5*num6
			//console.log(sum2)
		};
		let multi= act3(50,10)
		console.log(multi);
		
			

		

		

		function act4(num7,num8){
			//let sum3=  num7/num8
			console.log("display quotient of " + num7 + " and " + num8)
			//console.log(sum3);
			return num7/num8
		};
		let div1 = act4(50,10);
		console.log(div1);

		



	/*2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.*/

	function cir1(rad){
		console.log("the result of getting the area of a circle with "+ rad + " radius")
		return (3.1416 * 15)*15
	}
	let radCir = cir1(15)
	console.log(radCir);
	

	/*4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.*/
	function getAverage(mom1,mom2,mom3,mom4){
		console.log("the average of " + mom1 + " , "+mom2 +" , "+ mom3+ " and " + mom4 + " : ")
		return (mom1+mom2+mom3+mom4)/4
	}
	let average= getAverage(20,40,60,80);
	console.log(average)
	

	/*5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/
 
 function checkIfPassed(pass){
 	
 	console.log ("is " + pass + "/50 a passing score?")
 	return pass >= 37.5
 	 
 }
 let pass2= checkIfPassed(38)
 console.log(pass2)

